CREATE TABLE Songs
(
id int auto_increment not null,
Song varchar(255) NOT NULL,
Artist varchar(255),
ReleaseYear int(4),
PRIMARY KEY (id)
); 


INSERT INTO Songs (Song, Artist, ReleaseYear) 
VALUES ("I'll Be Missing You", "Puff Daddy", 1997),
("Say You, Say Me", "Lionel Richie", 1985),
("Play That Funky Music", "Wild Cherry", 1976),
("Gangsta's Paradise", "Coolio", 1996),
("Hot Stuff", "Donna Summer", 1979),
("I Love Rock 'n Roll", "Joan Jett & the Blackhearts", 1982), 
("Centerfold", "J. Geils Band Hot 100 Peak", 1982),
("Apologize", "Timberland", 2007),
("Tik Tok", "Ke$ha", 2010),
("Stayin' Alive", "Bee Gees", 1978);

CREATE TABLE Books 
(
id int auto_increment not null,
Title varchar(255),
Author varchar(255),
ReleaseYear int(4),
PRIMARY KEY (id)
); 

INSERT INTO Books (Title, Author, ReleaseYear) 
VALUES ("In Search of Lost Time", "Marcel Proust", 1913),
("Ulysses", "James Joyce", 1904),
("Don Quixote", "Miguel de Cervantes", 1851),
("Moby Dick", "Herman Melville", 1851),
("Hamlet", "William Shakespeare", 1600),
("War and Peace", "Leo Tolstoy", 1866),
("A Tale of Two Cities", "Charles Dickens", 1859),
("The Alchemist", "Paulo Coelho", 1988),
("Harry Potter and the Philosopher's Stone", "J.K. Rowling", 1997),
("The Hobbit", "J. R. R. Tolkien", 1937);


CREATE TABLE Highscore
(
	id int auto_increment not null,
    Name varchar(100) not null,
    Score int,
    PRIMARY KEY(id) 
);
