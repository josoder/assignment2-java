# README #

### Quiz server ###

* The poject implements a multithreaded server that asks the client music and book question. 
* Version 1.0

### How do I get set up? ###

* The questions are derived from a database, there is a sql-script file in the project folder named "JosoderQuiz.sql" run the script to set up the required tables and data. 
* Configuration: Since the application communicates with a database you need a local "db" running and to configure the "jdbc.properties" to match your set-up´s configurations. 
* Dependencies: junit, mysql, ormlite-core, ormlite-jbdc. They are all set up in the POM file. 

### Documentation ###

* The project has been set-up with mavens javadoc plugin and can easily be generated. 
