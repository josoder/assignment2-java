import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Class serves as a blueprint for entries in the book sql-table.
 * Made for the ormlite API.
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
@DatabaseTable(tableName = "Books")
public class Book {
    @DatabaseField(id=true)
    private int id;
    @DatabaseField(columnName = "Title")
    private String title;
    @DatabaseField(columnName = "Author")
    private String author;
    @DatabaseField(columnName = "ReleaseYear")
    private int releaseYear;

    public Book(){}

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

}
