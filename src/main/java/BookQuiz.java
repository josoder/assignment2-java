import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *  Implements a book quiz with question derived from a database.
 *  @author Joakim Söderstrand 2016-11-01
 *  @version 1.0
 */
public class BookQuiz implements Quiz {
    private Queue<Book> books;
    private Book currentBook;

    /**
     * Constructs an object functioning as a quiz with question derived from the parameter.
     * @param books  list of books
     */
    public BookQuiz(List<Book> books){
        this.books = new LinkedList<>();
        this.books.addAll(books);
    }

    /**
     * Creates a new question from the book-queue. Returns null if the queue is empty.
     * @return question
     */
    @Override
    public String nextQuestion() {
        if(books.size()==0){
            return null;
        }
        currentBook = books.poll();
        String question = "Who released the book: \"" + currentBook.getTitle() +
                "\" in year: " + currentBook.getReleaseYear() +
                "?";
        return question;
    }

    /**
     * Check if answer is correct, returns true if it is.
     * @param answer The answer given to the question.
     * @return boolean
     */
    @Override
    public boolean checkAnswer(String answer) {
        return answer.equalsIgnoreCase(correctAnswer());
    }

    /**
     * Returns the correct answer of the current question.
     * @return String
     */
    @Override
    public String correctAnswer() {
        return currentBook.getAuthor();
    }
}
