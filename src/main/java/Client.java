import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

/**
 * Implements a client that connects to a quiz-server.
 * The client answers questions and gets a score at the end of the rounds.
 * @author Joakim Södersrand 2016-11-02
 * @version 1.0
 */
public class Client extends Server
{
    private Scanner scanner;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private String name;


    /**
     * Create a new client-instance, connects to the hostname and port specified.
     * Initializes in- and output streams and enters the loop of messages between server and client.
     * @param host the hostname of the server
     * @param port the port number of the server
     */
    public Client(String host, int port) {
        scanner = new Scanner(System.in);
        try {
            this.socket = new Socket(host, port);
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());
            //startConversation();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enters the message loop.
     * The client is firstly asked to present it's name to the server, if the name is unavailable(in use by another
     * client connected to the server) the client will be asked to choose another nick.
     * The loop will continue to receive question until the quiz finished or when the client enters "quit".
     */
    public void startConversation() {
        // Firstly
        handShake();
        String welcome = receiveMessage();
        String question, result;
        System.out.println(welcome);
        // start the message loop
        while (true) {
            // get a new question
            question = receiveMessage();
            if(question.equalsIgnoreCase("finished")) break;
            System.out.println(question);
            // answer the question
            String answer = scanner.nextLine();
            if(answer.equalsIgnoreCase("quit")) {
                sendMessage("quit");
                disconnect();
                return;
            }
            sendMessage(answer);
            // Get the result
            result = receiveMessage();
            if(result.equalsIgnoreCase("finished")) break;
            System.out.println(result);
        }
        String endResult = receiveMessage();
        // Print the end result
        System.out.println(endResult);
        // Print highscore
        System.out.println(receiveMessage());

        disconnect();
    }



    /**
     * Start the conversation with the server, tell the server the name of the client.
     * If the name is currently unavailable, the client will be asked to pick a new nick.
     */
    public void handShake() {
        try {
            // listen to welcome message
            System.out.println(in.readUTF());
            String name;
            // tell the name of the client to the server, repeat if the name is in use.
            while (true) {
                name = getNameFromInput();
                sendMessage(name);
                boolean available = verify();
                if(available) break;
                else {
                    System.out.println(receiveMessage());
                }
            }
            this.name = name;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read boolean from server.
     */
    public boolean verify(){
        try {
            return in.readBoolean();
        }
        catch (IOException e){
            System.out.println(e);
        }
        return false;
    }

    /**
     * Send message to server
     * @param m the message to be sent to the server
     */
    public void sendMessage(String m){
        try {
            out.writeUTF(m);
            out.flush();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Get incoming message from the server
     * @return
     */
    public String receiveMessage(){
        String message;
        try {
            message = in.readUTF();
            return message;
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the users name from input, makes sure it´s minimum 4 characters long.
     * @return String name
     */
    private String getNameFromInput() {
        String name = null;
        while (true) {
            name = scanner.nextLine().trim();
            if (name.length() >= 4) break;
            else {
                System.out.println("The nickname must be minimum 4 characters, please try again!");
            }
        }
        return name;
    }

    /**
     * Close the connection and the components used for the communication.
     */
    public void disconnect() {
        try {
            in.close();
            out.close();
            socket.close();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        System.out.println("Closing client");
    }

    /**
     * Main method to start the client
     * @param args
     */
    public static void main(String[] args) {
        Client c= new Client("localhost", 8000);
        c.startConversation();
    }
}
