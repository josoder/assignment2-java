import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * ClientThread handles the conversation between the server and the clients, every connected client
 * has a ClientThread running.
 * The ClientThread receives the name of the client and keeps track of all the nicknames currently connected.
 * @author  Joakim Söderstrand 2016-11-17
 * @version 1.0
 */
public class ClientThread implements Runnable {
    private String name;
    private int score;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private int qNr = 1;
    private QuizFactory quizFactory;
    private boolean running=true;
    private static CopyOnWriteArrayList<String> names;

    /**
     * Creates a new chat instance.
     * @param socket
     */
    public ClientThread(Socket socket) {
        names = new CopyOnWriteArrayList<>();
        this.socket = socket;
    }

    /**
     * Set the clients name
     * @param name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Implements the client handling when server is started. Instantiates the streams for I/O, gets the name
     * of the client and starts the first quiz.
     */
    public void run() {
        try {
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            // Firstly get the name of the client
            getClientName();
            names.add(this.name);
            // Start the quiz
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The start method, starts up the quiz.
     */
    public void start() {
        quizFactory = new QuizFactory();
        Quiz musicQuiz = quizFactory.getMusicQuiz();
        Quiz bookQuiz = quizFactory.getBookQuiz();

        // Welcome client
        sendMessage("Welcome to the quiz, " + name + ". To quit, type quit and press enter." +
                " starting first round");
        startQuiz(musicQuiz);
        if(!running) return;
        sendMessage("You finished with " + score + " points. Want to play another one with book Questions? Y/N");
        String answer = receiveMessage();
        if(answer.equalsIgnoreCase("Y")){
            sendMessage("Starting round 2");
            startQuiz(bookQuiz);
        }

        sendMessage("finished");

        writeToHighScore();
        printHighScore();
        disconnect();
    }

    /**
     * Start the type of quiz given as a parameter.
     * The quiz asks the client question and checks if the received answer is correct, it then prints the result
     * to the client.
     * @param quiz
     */
    public void startQuiz(Quiz quiz) {
        String answer, question;

        while (running){
            question = quiz.nextQuestion();
            if(question == null) break;
            sendMessage("Question nr" + qNr + "\n" + question);
            answer = receiveMessage();
            System.out.println(answer);
            if (answer.equalsIgnoreCase("quit")) {
                disconnect();
            } else if (quiz.checkAnswer(answer)) {
                score++;
                sendMessage("correct, 1 point was added to your score");
            } else {
                sendMessage("Sry wrong answer, the answer should have been " + quiz.correctAnswer());
            }
            qNr++;
        }
    }

    public String getName(){
        return this.name;
    }

    /**
     * Welcome the new client and ask for its name, sends a boolean value letting it know if the name is available.
     */
    public void getClientName() {
        try {
            sendMessage("Hello and welcome to the quiz, please enter your nickname to continue.(minimum 4 characters)");
            String name;
            boolean available;
            while (true) {
                name = in.readUTF();
                available = nameAvailable(name);
                out.writeBoolean(available);
                out.flush();
                if(available) break;
                else sendMessage("the name is already in use!");
            }
            setName(name);
        } catch (IOException e) {
            if(!running) System.out.println("Session was shut down" + e);
            else  System.out.println("Something went wrong while sending message to client" + e);
            e.printStackTrace();
        }
    }

    /**
     * Check if name is available, returns true if it is.
     */
    public boolean nameAvailable(String name) {
        for(String s:names){
            if(s.equalsIgnoreCase(name)) return false;
        }
        return true;
    }

    /**
     * Send message to client
     * @param message
     */
    public void sendMessage(String message) {
        try {
            out.writeUTF(message);
            out.flush();
        } catch (IOException e) {
            if(!running) System.out.println("Session was shut down" + e);
            else  System.out.println("Something went wrong while sending message to client" + e);
        }
    }

    /**
     * Get message from client
     * @return message
     */
    public String receiveMessage() {
        try {
            String message = in.readUTF();
            return message;
        } catch (IOException e) {
            if(!running) System.out.println("Session was shut down" + e);
            else  System.out.println("Something went wrong while receiving message from client" + e);
        }
        return null;
    }

    /**
     * Print the top 10 high-score out of the database.
     */
    public void printHighScore() {
        List<HighScore> highScores = DBHandler.selectAllScores();
        String topScores="Top 10 scores \n" ;
        int i=1;
        for(HighScore score : highScores){
            topScores += i + ": " + score.getName() + " " + score.getScore() + "\n";
            i++;
            if(i>10) break;
        }
        sendMessage(topScores);
    }

    /**
     * Inserts a new entry to the high-score database.
     */
    public void writeToHighScore(){
        sendMessage("You finished with " + score + " points.");
        HighScore highScore = new HighScore(this.name, this.score);
        DBHandler.insertScore(highScore);
    }

    /**
     * Clean up, close streams and sockets.
     */
    public void disconnect(){
        try {
            this.in.close();
            this.out.close();
            this.socket.close();
            this.running = false;
            System.out.println(name + " was disconnected from the server");
            running = false;
            Server.removeClient(this);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}