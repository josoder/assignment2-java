import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * DBHandler implements useful functions towards the database using the ORMLite API.
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
public class DBHandler {
    private Dao<Song, Integer> songDao;
    private static Dao<HighScore, Integer> highScoreDao;
    private static QueryBuilder<HighScore, Integer> highScoreQueryBuilder;
    private Dao<Book, Integer> bookDao;
    private QueryBuilder<Song, Integer> songQueryBuilder;
    private QueryBuilder<Book, Integer> bookQueryBuilder;
    private ConnectionSource connectionSource;

    /**
     * Constructor.
     * Instantiates the DAOs and the connection
     */
    public DBHandler(){
        try {
            connectionSource = JDBCConnection.getConnection();
            songDao = DaoManager.createDao(connectionSource,Song.class);
            highScoreDao = DaoManager.createDao(connectionSource, HighScore.class);
            bookDao = DaoManager.createDao(connectionSource, Book.class);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Select all books from the book table and returns them as a list.
     * Returns null if unsuccessful
     * @Return List<Book>
     */
    public List<Book> selectAllBooks() {
        try{
            bookQueryBuilder = bookDao.queryBuilder();
            List<Book> books = bookQueryBuilder.query();
            return books;
        }
        catch (SQLException e){
            System.out.println("Something went wrong while trying to get data from Books" + e);
        }
        return null;
    }

    /**
     * Select all songs from the Song table and returns them as a list.
     * Returns null if unsuccessful
     * @Return List<Song>
     */
    public List<Song> selectAllSongs(){
        try{
            songQueryBuilder = songDao.queryBuilder();
            List<Song> songs = songQueryBuilder.query();
            return songs;
        }
        catch (SQLException e){
            System.out.println("Something went wrong while trying to get data from Songs" + e);
        }
        return null;
    }

    /**
     * Insert a new entry to the high-score table.
     * If the current name exist with the same score it is overwritten.
     */
    public static void insertScore(HighScore hsObj){
        try{
            highScoreQueryBuilder = highScoreDao.queryBuilder();
            highScoreQueryBuilder.where().eq("Name", hsObj.getName()).and().eq("Score", hsObj.getScore());
            HighScore hs = highScoreQueryBuilder.queryForFirst();
            if(hs!=null){
                deleteFromHighScore(hs);
            }
            highScoreDao.create(hsObj);
        }
        catch (SQLException e){
            System.out.println("Something went wrong while trying to insert score to DB" + e);
        }
    }

    /**
     * Get all the scores in the high-score table in a list with ascending order.
     */
    public static List<HighScore> selectAllScores(){
        try{
            highScoreQueryBuilder = highScoreDao.queryBuilder();
            highScoreQueryBuilder.orderBy("Score", false);
            List<HighScore> scores = highScoreQueryBuilder.query();
            return scores;
        }
        catch (SQLException e){
            System.out.println("Something went wrong while trying to get data from highscore" + e);
        }
        return null;
    }

    /**
     * Remove entry from the high-score table.
     */
    public static void deleteFromHighScore(HighScore hsObject){
        try{
            DeleteBuilder deleteHS = highScoreDao.deleteBuilder();
            deleteHS.where().eq("Name", hsObject.getName());
            deleteHS.delete();
        }
        catch (SQLException e){
            System.out.println("Something went wrong while trying to delete from highscore" + e);
        }
    }

}
