import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Class serves as a blueprint for entries in the highscore sql-table.
 * Made for the ormlite API.
 * @author Joakim Söderstrand 2016-11-17
 * @version 1.0
 */
@DatabaseTable(tableName = "Highscore")
public class HighScore {
    @DatabaseField(id=true)
    private int id;
    @DatabaseField(columnName = "Name")
    private String name;
    @DatabaseField(columnName = "Score")
    private int score;

    public HighScore(){}

    public HighScore(String name, int score){
        this.name = name;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return this.name;
    }
}