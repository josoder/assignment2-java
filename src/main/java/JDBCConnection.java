import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Initializes connection to a database using a jdbc connection source, to be used by the ORMlite API.
 * Properties for the connection is derived from the file "jdbc.properties" located in the assignment2 folder.
 * Edit this file to match your set-up's configurations.
 * @author Joakim Söderstrand 2016-11-02
 * @version 1.0
 */
public class JDBCConnection
{
    private static String PROPSFILENAME = "jdbc.properties";
    private static Properties props;
    private static JdbcConnectionSource connectionSource;

    /**
     * Should not be instantiated.
     */
    private JDBCConnection(){}


    /**
     * Initializes the connectionSource.
     */
    public static void initConnection(){
        props = new Properties();
        try {
            props.load(new FileInputStream(PROPSFILENAME));
            String user, password, dbhost, dbname, dbtype, portNr;
            user = props.getProperty("user");
            password = props.getProperty("password");
            dbhost = props.getProperty("dbhost");
            dbname = props.getProperty("dbname");
            dbtype = props.getProperty("dbtype");
            portNr = props.getProperty( "port");

            if (user.length() == 0 || dbhost.length() == 0 || dbname.length() == 0 || dbtype.length() == 0 ||
                    portNr.length() == 0) {
                throw new IllegalArgumentException(PROPSFILENAME + " contains illegal values");
            }
            String url = "jdbc:" + dbtype + "://" + dbhost + ":" + portNr + "/" + dbname;
            connectionSource = new JdbcConnectionSource(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (IOException e){
            System.out.println("Something went wrong while initializing connection" + e) ;
        }
    }

    /**
     * Return the connection.
     * @Return connectionSource com.j256.ormlite.jdbc.JdbcConnectionSource
     */
    public static ConnectionSource getConnection() {
        if(connectionSource == null) initConnection();
        return connectionSource;
    }

    /**
     * Close the connection.
     */
    public static void closeConnection() {
        if(connectionSource == null) return;
        try {
            connectionSource.close();
        }
        catch (IOException e){
            System.out.println("Something went wrong while closing connection" + e);
        }
    }
}
