import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Implements a music quiz with question derived from the music database.
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
public class MusicQuiz implements Quiz {
    private Queue<Song> songs;
    private Song currentSong;

    /**
     * Constructs an object functioning as a quiz with question derived from the parameter.
     * @param songs list of songs
     */
    public MusicQuiz(List<Song> songs){
        this.songs = new LinkedList<>();
        this.songs.addAll(songs);
    }

    /**
     * Creates a new question from the song-queue. Returns null if the queue is empty.
     * @return question
     */
    @Override
    public String nextQuestion() {
        if(songs.size()==0){
            return null;
        }
        currentSong = songs.poll();
        String question = "Who released the song: \"" + currentSong.getSong() +
                "\" in year: " + currentSong.getReleaseYear() +
                "?";
        return question;
    }

    /**
     * Check if answer is correct, returns true if it is.
     * @param answer The answer given to the question.
     * @return boolean
     */
    @Override
    public boolean checkAnswer(String answer) {
        return answer.equalsIgnoreCase(correctAnswer());
    }

    /**
     * Returns the correct answer of the current question.
     * @return String
     */
    @Override
    public String correctAnswer() {
        return currentSong.getArtist();
    }
}
