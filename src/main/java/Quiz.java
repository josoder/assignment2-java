import java.util.List;
import java.util.Queue;

/**
 * Interface holding methods that needs to be implemented for Quiz-classes in this project.
 * @author Joakim Söderstrand 2016-11-03
 * @version 1.0
 */
public interface Quiz {
    // returns a random question
    String nextQuestion();
    // returns true if the answer was correct
    boolean checkAnswer(String answer);
    // returns the correct answer as a string
    String correctAnswer();
}
