import java.util.List;

/**
 * Created by Joakim Söderstrand on 2016-11-01 email : sodjoa15@student.westerdals.no
 * QuizFactory is a Factory-class creating different types of quiz-objects. Using data from the local database.
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
public class QuizFactory
{
    private static DBHandler dbHandler=null;

    /**
     * Constructor
     */
    public QuizFactory(){
        if(dbHandler==null) dbHandler = new DBHandler();
    }

    /**
     * Returns a quiz with music questions.
     * @return MusicQuiz
     * @see MusicQuiz
     */
    public Quiz getMusicQuiz(){
        List<Song> songs = dbHandler.selectAllSongs();
        return new MusicQuiz(songs);
    }

    /**
     * Returns a quiz with book questions.
     * @return BookQuiz
     * @see BookQuiz
     */
    public Quiz getBookQuiz(){
        List<Book> books = dbHandler.selectAllBooks();
        return new BookQuiz(books);
    }


}
