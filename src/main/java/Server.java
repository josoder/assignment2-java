import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Created by Joakim Söderstrand on 2016-11-01 email sodjoa15@student.westerdals.no
 * Implements a simple multithreaded server that listens for client connection and creates
 * new chat instances using ClientThread.
 * Default port is 8000, it can be changed with setPort() before the server is started.
 * @see ClientThread
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
public class Server  implements Runnable {
    private static int port = 8000;
    private static boolean running = true;
    private static Thread listen;
    private static CopyOnWriteArrayList<ClientThread> clientThreads;
    private static ServerSocket serverSocket;

    /**
     * Creates a new server-instance
     */
    public Server() {
        clientThreads = new CopyOnWriteArrayList<>();
    }

    /**
     * Set the port number of the server.
     * @param port nr of the port
     */
    public void setPort(int port){
        this.port = port;
    }

    /**
     * Remove a client when they are disconnected from a session.
     */
    public static void removeClient(ClientThread c){
        clientThreads.remove(c);
    }

    /**
     * Start the thread listening for incoming connections.
     */
    public void startServer(){
        running = true;
        listen = new Thread(new Server());
        listen.start();
    }

    /**
     * Stop the server.
     * Disconnects all clients,
     */
    public void shutdDown(){
        if(clientThreads.size()>0) for(ClientThread ct : clientThreads){
            ct.disconnect();
        }
        System.out.println("Shutting down the server");
        try{
            running = false;
            serverSocket.close();
            JDBCConnection.closeConnection();
        }
        catch (IOException e){
            System.out.println("Something went wrong while shutting down server");
        }
    }

    /**
     * Listen for new connections
     */
    @Override
    public void run() {
        try {
            this.serverSocket = new ServerSocket(port);
            System.out.println("QuizFactory server started at " + new Date());
            System.out.println("To shut down the server type quit and press enter");
            while (running){
                Socket socket = serverSocket.accept();
                ClientThread newClient = new ClientThread(socket);
                Thread clientThread = new Thread(newClient);
                clientThreads.add(newClient);
                clientThread.start();
            }
        } catch (IOException e) {
            if(!running) System.out.println("Server was shut down");
            else {
                e.printStackTrace();
            }
        }
    }

    /**
     * Client code to start the server.
     */
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Server server = new Server();
        server.startServer();
        while(true){
            String command = sc.nextLine();
            if(command.equalsIgnoreCase("quit")) {
                server.shutdDown();
                break;
            }
        }
    }
}