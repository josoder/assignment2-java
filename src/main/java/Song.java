import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Class serves as a blueprint for entries in the song sql-table.
 * Made for the ormlite API.
 * @author Joakim Söderstrand 2016-11-01
 * @version 1.0
 */
@DatabaseTable(tableName = "Songs")
public class Song {
    @DatabaseField(id=true)
    private int id;
    @DatabaseField(columnName = "Song")
    private String song;
    @DatabaseField(columnName = "Artist")
    private String artist;
    @DatabaseField(columnName = "Releaseyear")
    private int releaseYear;

    public Song(){}

    public int getId(){
        return this.id;
    }

    public String getSong(){
        return this.song;
    }

    public String getArtist(){
        return this.artist;
    }

    public int getReleaseYear(){
        return this.releaseYear;
    }
}
