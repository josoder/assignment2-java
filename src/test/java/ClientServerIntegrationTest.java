import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by Joakim Söderstrand on 2016-11-01 email : sodjoa15@student.westerdals.no
 * Integration test of the client and server.
 */
public class ClientServerIntegrationTest {
    private static Server server;

    @BeforeClass
    public static void setUp() {
        server = new Server();
        server.startServer();
    }

    /**
     * Testing the initial handshake between the server and clients.
     * Connecting two clients testing their send and receive methods.
     */
    @Test
    public void testHandshake(){
        Client test = new Client("localhost", 8000);
        Client test2 = new Client("localhost", 8000);
        // Receive welcome message from server
        System.out.println(test.receiveMessage());
        System.out.println(test2.receiveMessage());
        // tell the clients name
        test.sendMessage("testName");
        test2.sendMessage("test2Name");
        // If the name is not currently in use the method will return true
        Assert.assertTrue(test.verify());
        Assert.assertTrue(test2.verify());
        System.out.println(test.receiveMessage());
        System.out.println(test2.receiveMessage());


        test.disconnect();
        test2.disconnect();
    }

    @AfterClass
    public static void tearDown() {
        server.shutdDown();
    }
}
