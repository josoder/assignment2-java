import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.junit.*;

/**
 * Created by Joakim Söderstrand on 2016-11-01 email : sodjoa15@student.westerdals.no
 * Testing the JDBCConnection class.
 * @see JDBCConnection
 *
 */
public class DBConnectionTest
{
    private JDBCConnection jdbcConnection;
    private ConnectionSource connectionSource;


    /**
     * Try to get a connection source derived from the property files.
     */
    @Test
    public void getConnection(){
        connectionSource = jdbcConnection.getConnection();
    }

    /**
     * Close the connection.
     */
    @Test
    public void closeConnection(){
        JDBCConnection.closeConnection();
    }
}
