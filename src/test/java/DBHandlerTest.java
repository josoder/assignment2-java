import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joakim Söderstrand on 2016-11-01 email : sodjoa15@student.westerdals.no
 * Tests the methods in DBHandler.
 * @see DBHandler
 */
public class DBHandlerTest {
    private static DBHandler handler;
    private static ArrayList<HighScore> test;

    @BeforeClass
    public static void setUp() {
        handler = new DBHandler();
        test = new ArrayList<>();
        for(int i=0; i<10; i++) {
            test.add(new HighScore("test " + i, i));
        }

    }

    /**
     * Selects all the songs from the Song-table.
     * The list returned should be of size ten, since the script used to create the db has 10 rows inserted.
     */
    @Test
    public void selectAllSongs(){
        List<Song> songs = handler.selectAllSongs();
        for(Song s:songs){
            System.out.print(s.getId() + ":" + s.getSong() + " " + s.getArtist() + " Released:" + s.getReleaseYear());
            System.out.print("\n");
        }
        Assert.assertTrue(songs.size() == 10);
    }

    /**
     * Try to insert new scores to the highscore-table.
     */
    @Test
    public void insertScoreToHighScore(){
        int before = handler.selectAllScores().size();
        for (HighScore hs:test){
            handler.insertScore(hs);
        }
        int after = handler.selectAllScores().size();
        Assert.assertTrue(after == before + test.size());
    }

    /**
     * Select all the high-scores from the table.
     */
    @Test
    public void selectAllHighScores(){
        List<HighScore> scores = handler.selectAllScores();
        for(HighScore hs:scores){
            System.out.println(hs.getName() + " " + hs.getScore());
        }
    }

    /**
     * Select all the books from the book-table.
     * Assert that the list received is of size 10.
     */
    @Test
    public void selectAllBooks(){
        List<Book> books = handler.selectAllBooks();
        for(Book b:books){
            System.out.print(b.getId() + " " +  b.getTitle() + " " + b.getAuthor() + " " + b.getReleaseYear() +
                    "\n");
        }
    }

    /**
     * Remove the test data and close the connection.
     */
    @AfterClass
    public static void cleanUp(){
        for (HighScore hs: test) {
            handler.deleteFromHighScore(hs);
        }
        JDBCConnection.closeConnection();
    }


}
