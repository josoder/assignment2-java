/**
 * Created by jsode on 2016-11-07.
 */
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by Joakim Söderstrand on 2016-11-01 email : sodjoa15@student.westerdals.no
 * Testing the QuizFactory class and the different kinds of quizzes currently implemented.
 * @see QuizFactory
 */
public class QuizFactoryTest {
    private static QuizFactory qf;

    @BeforeClass
    public static void setUp(){
        qf = new QuizFactory();
    }

    /**
     * Get a new musicQuiz instance and test the checkAnswer method.
     * The String passed is the correct answer so it should return true.
     */
    @Test
    public void getMusicQuiz(){
        Quiz musicQuiz = qf.getMusicQuiz();
        System.out.println(musicQuiz.nextQuestion());
        String answer = musicQuiz.correctAnswer();
        Assert.assertTrue(musicQuiz.checkAnswer(answer));
    }

    /**
     * Same as for the musicQuiz, but with books this time.
     */
    @Test
    public void getBookQuiz(){
        Quiz bookQuiz = qf.getBookQuiz();
        System.out.println(bookQuiz.nextQuestion());
        String answer = bookQuiz.correctAnswer();
        Assert.assertTrue(bookQuiz.checkAnswer(answer));
    }

}